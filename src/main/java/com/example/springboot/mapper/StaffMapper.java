package com.example.springboot.mapper;

import com.example.springboot.model.Staff;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface StaffMapper {


    final String getAll = "SELECT * FROM STAFF";
    final String getById = "SELECT * FROM STAFF WHERE ID = #{id}";
    final String getByTitle = "SELECT * FROM STAFF WHERE TITLE = #{title}";
    final String deleteById = "DELETE from STAFF WHERE ID = #{id}";
    final String insert = "INSERT INTO STAFF (STAFF_NAME) VALUES (#{staffName})";
    final String update = "UPDATE ITEM SET STAFF_NAME = #{staffName} WHERE ID = #{id}";

    @Select(getAll)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    List<Staff> getAll();

    @Select(getById)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    Staff getById(int id);

    @Select(getByTitle)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    Staff getByTitle(String title);

    @Update(update)
    void update(Staff staff);

    @Delete(deleteById)
    void delete(int id);

    @Insert(insert)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Staff staff);

}
