package com.example.springboot.mapper;

import com.example.springboot.model.Assignment;
import com.example.springboot.model.Item;
import com.example.springboot.model.Staff;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface AssignmentMapper {


    final String getAll = "SELECT * FROM ASSIGNMENT";
    final String getById = "SELECT * FROM ASSIGNMENT WHERE ID = #{id}";
    final String getItemByStaffId = "SELECT ITEM_ID, ITEM_NAME FROM ASSIGNMENT WHERE STAFF_ID = #{id}";
    final String getStaffByItemId = "SELECT STAFF_ID, STAFF_NAME FROM ASSIGNMENT WHERE ITEM_ID = #{id}";
    final String deleteById = "DELETE from ASSIGNMENT WHERE ID = #{id}";
    final String insert = "INSERT INTO ASSIGNMENT (ITEM_ID, STAFF_ID, ITEM_NAME, STAFF_NAME ) VALUES (#{itemId}, #{staffId}, #{itemName}, #{staffName})";
    final String update = "UPDATE ASSIGNMENT SET AVAILABLE_STATUS = #{availableStatus} WHERE TITLE = #{title}";

    @Select(getAll)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "itemId", column = "ITEM_ID"),
            @Result(property = "staffId", column = "STAFF_ID"),
            @Result(property = "itemName", column = "ITEM_NAME"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    List<Assignment> getAll();

    @Select(getById)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "itemId", column = "ITEM_ID"),
            @Result(property = "staffId", column = "STAFF_ID"),
            @Result(property = "itemName", column = "ITEM_NAME"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    Assignment getById(int id);

    @Select(getItemByStaffId)
    @Results(value = {
            @Result(property = "id", column = "ITEM_ID"),
//            @Result(property = "itemId", column = "ITEM_ID"),
            @Result(property = "itemName", column = "ITEM_NAME")
    })
    List<Item> getItemByStaffId(Long staffId);

    @Select(getStaffByItemId)
    @Results(value = {
            @Result(property = "id", column = "STAFF_ID"),
//            @Result(property = "staffId", column = "STAFF_ID"),
            @Result(property = "staffName", column = "STAFF_NAME")
    })
    List<Staff> getStaffByItemId(Long itemId);

    @Update(update)
    void update(String title, boolean availableStatus);

    @Delete(deleteById)
    void delete(int id);

    @Insert(insert)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Assignment assignment);
}
