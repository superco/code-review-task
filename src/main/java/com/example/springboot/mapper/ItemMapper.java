package com.example.springboot.mapper;

import com.example.springboot.model.Item;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ItemMapper {


    final String getAll = "SELECT * FROM ITEM";
    final String getById = "SELECT * FROM ITEM WHERE ID = #{id}";
    final String getByItemId = "SELECT * FROM ITEM WHERE ID = #{ID}";
    final String deleteById = "DELETE from ITEM WHERE ID = #{id}";
    final String insert = "INSERT INTO ITEM (ITEM_NAME) VALUES (#{itemName})";
    final String update = "UPDATE ITEM SET ITEM_NAME = #{itemName} WHERE ID = #{id}";

    @Select(getAll)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "itemName", column = "ITEM_NAME")
    })
    List<Item> getAll();

    @Select(getById)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "itemName", column = "ITEM_NAME")
    })
    Item getById(int id);

    @Select(getByItemId)
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "itemName", column = "ITEM_NAME")
    })
    Item getByItemId(int id);

    @Update(update)
    void update(Item item);

    @Delete(deleteById)
    void delete(int id);

    @Insert(insert)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Item item);

}
