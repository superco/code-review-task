package com.example.springboot.controller;

import com.example.springboot.mapper.AssignmentMapper;
import com.example.springboot.mapper.StaffMapper;
import com.example.springboot.model.Assignment;
import com.example.springboot.model.Items;
import com.example.springboot.model.Staff;
import com.example.springboot.model.Staffs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/staff")
public class StaffController {
    @Autowired
    StaffMapper staffMapper;
    @Autowired
    AssignmentMapper assignmentMapper;

    @PostMapping("add_staff")
    ResponseEntity<?> addStaff(@RequestBody Staff staff) {
        staffMapper.insert(staff);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("edit_staff")
    ResponseEntity<?> editStaff(@RequestBody Staff staff) {
        staffMapper.update(staff);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("delete_staff/{id}")
    ResponseEntity<?> deleteStaff(@PathVariable int id) {
        staffMapper.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("get_all")
    ResponseEntity<?> getAll() {
        return new ResponseEntity<>(staffMapper.getAll(), HttpStatus.OK);
    }

    @GetMapping("assigned_item/{id}")
    ResponseEntity<?> assignedItem(@PathVariable long id) {
        return new ResponseEntity<>(assignmentMapper.getItemByStaffId(id), HttpStatus.OK);
    }

    @GetMapping("report_staff")
    ResponseEntity<?> reportStaff() {
        ArrayList<Staffs> staffsArrayList = new ArrayList<>();
        List<Assignment> assignmentList = assignmentMapper.getAll();
        Staffs staffs = new Staffs();
        Staffs staffCounter = new Staffs();
        for (Assignment assignment :
                assignmentList) {
            if (staffCounter.getId() != assignment.getStaffId()) {
                staffs.setId(assignment.getStaffId());
                staffs.setStaffName(assignment.getStaffName());
                staffs.setItemList(assignmentMapper.getItemByStaffId(assignment.getStaffId()));
                staffsArrayList.add(staffs);
                staffCounter = staffs;
            }
            staffs = new Staffs();
        }
        return new ResponseEntity<>(staffsArrayList, HttpStatus.OK);
    }

    @GetMapping("report_item")
    ResponseEntity<?> reportItem() {
        ArrayList<Items> itemsArrayList = new ArrayList<>();
        List<Assignment> assignmentList = assignmentMapper.getAll();
        Items items = new Items();
        Items itemsCounter = new Items();
        for (Assignment assignment :
                assignmentList) {
            if (items == null || items.getId() != assignment.getStaffId()) {
                items.setId(assignment.getStaffId());
                items.setItemName(assignment.getStaffName());
                items.setStaffList(assignmentMapper.getStaffByItemId(assignment.getStaffId()));
                itemsArrayList.add(items);
                itemsCounter = items;
            }
            items = new Items();
        }
        return new ResponseEntity<>(itemsArrayList, HttpStatus.OK);
    }
}
