package com.example.springboot.controller;

import com.example.springboot.mapper.AssignmentMapper;
import com.example.springboot.mapper.ItemMapper;
import com.example.springboot.mapper.StaffMapper;
import com.example.springboot.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {
    @Autowired
    ItemMapper itemMapper;
    @Autowired
    StaffMapper staffMapper;
    @Autowired
    AssignmentMapper assignmentMapper;

    @PostMapping("add_item")
    ResponseEntity<?> addItem(@RequestBody Item item) {
        itemMapper.insert(item);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("edit_item")
    ResponseEntity<?> editItem(@RequestBody Item item) {
        itemMapper.update(item);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("delete_item/{id}")
    ResponseEntity<?> deleteItem(@PathVariable int id) {
        itemMapper.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("get_all")
    ResponseEntity<?> getAll() {
        return new ResponseEntity<>(itemMapper.getAll(), HttpStatus.OK);
    }

    @PostMapping("assign_item")
    ResponseEntity<?> assignItem(@RequestBody Assignment assignment) {
        if (assignment.getStaffId() == staffMapper.getById((int) Long.parseLong(assignment.getStaffId().toString())).getId()
        && assignment.getItemId() == itemMapper.getById((int) Long.parseLong(assignment.getItemId().toString())).getId()) {
            Assignment addAssignment = new Assignment();
            addAssignment.setStaffId(assignment.getStaffId());
            addAssignment.setItemId(assignment.getItemId());
            addAssignment.setStaffName(staffMapper.getById((int) Long.parseLong(assignment.getStaffId().toString())).getStaffName());
            addAssignment.setItemName(itemMapper.getById((int) Long.parseLong(assignment.getItemId().toString())).getItemName());
            assignmentMapper.insert(addAssignment);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Staff or Item not found", HttpStatus.CONFLICT);
    }

    @GetMapping("report_staff")
    ResponseEntity<?> reportStaff() {
        ArrayList<Staffs> staffsArrayList = new ArrayList<>();
        List<Assignment> assignmentList = assignmentMapper.getAll();
        Staffs staffs = new Staffs();
        Staffs staffCounter = new Staffs();
        for (Assignment assignment :
                assignmentList) {
            if (staffCounter.getId() != assignment.getStaffId()) {
                staffs.setId(assignment.getStaffId());
                staffs.setStaffName(assignment.getStaffName());
                staffs.setItemList(assignmentMapper.getItemByStaffId(assignment.getStaffId()));
                staffsArrayList.add(staffs);
                staffCounter = staffs;
            }
            staffs = new Staffs();
        }
        return new ResponseEntity<>(staffsArrayList, HttpStatus.OK);
    }

    @GetMapping("report_item")
    ResponseEntity<?> reportItem() {
        ArrayList<Items> itemsArrayList = new ArrayList<>();
        List<Assignment> assignmentList = assignmentMapper.getAll();
        Items items = new Items();
        Items itemsCounter = new Items();
        for (Assignment assignment :
                assignmentList) {
            if (items == null || items.getId() != assignment.getStaffId()) {
                items.setId(assignment.getStaffId());
                items.setItemName(assignment.getStaffName());
                items.setStaffList(assignmentMapper.getStaffByItemId(assignment.getStaffId()));
                itemsArrayList.add(items);
                itemsCounter = items;
            }
            items = new Items();
        }
        return new ResponseEntity<>(itemsArrayList, HttpStatus.OK);
    }
}
